//
// main.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Thu Feb  9 15:00:18 2017 Linsey Benoît
// Last update Sun Mar  5 16:05:15 2017 Anthony Jouvel
//

# include "parser_file.hpp"
# include "exception.hpp"
# include "Runtime.hpp"

static int		process(char **args, std::ifstream& file)
{
  std::string		line;
  ParserFile		parser(args);
  nts::t_ast_node	*root;

  while (std::getline(file, line))
    parser.feed(line);

  root = parser.createTree();
  try
    {
      parser.parseTree(*root);
    }
  catch (BadParamError &e)
    {
      e.checkFile(args[0]);
      throw e;
    }
  Runtime		run_process = Runtime(parser.getOut(),
					      parser.getComp(),
					      parser.getCl(),
					      parser.getIn());
  run_process.run();
  return (0);
}

int		main(int ac, char **av)
{
  std::ifstream file;

  try
    {
      if (ac < 2)
	throw BadParamError(BadParamError::M_FILE, av[0]);
      file.open(av[1], std::fstream::in);
      if (file)
	return (process(&av[1], file));
      else
	throw BadParamError(BadParamError::B_FILE, av[1]);
    }
  catch (const std::exception& e)
    {
      std::cerr << e.what() << std::endl;
    }
  return (0);
}

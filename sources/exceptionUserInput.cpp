//
// exceptionUserInput.cpp for  in /home/anthony/rendu/cpp_nanotekspice
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Thu Feb 16 15:44:31 2017 Anthony Jouvel
// Last update Thu Feb 16 16:52:31 2017 Anthony Jouvel
//

# include "exceptionUserInput.hpp"
# include "Runtime.hpp"

UserInputError::UserInputError(std::string const& wrongInput) throw()
{
  std::ostringstream	oss;

  oss << "Expected : \"";
  for (auto it : Runtime::userInput)
    {
      oss << "[" << it.first << "]";
    }
  oss << "\" But have \"" << wrongInput << "\"";
  this->_message = oss.str();
}

const char		*UserInputError::what() const throw()
{
  return (this->_message.c_str());
}

//
// Comp4001.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Sat Feb 18 09:52:56 2017 Linsey Benoît
// Last update Sat Mar  4 16:52:21 2017 Anthony Jouvel
//

# include "Comp4001.hpp"

nts::Tristate		Comp4001::Compute(size_t pin_num_this)
{
  nts::Tristate		ret = nts::UNDEFINED;

  if (pin_num_this > 13 || pin_num_this == 7)
    throw std::runtime_error("pin does not exist");

  if (_temp[pin_num_this - 1].second == true)
    return (_temp[pin_num_this - 1].first);

  _temp[pin_num_this - 1].second = true;

  if (pin_num_this == 3 || pin_num_this == 10)
    ret = nor_gate(Compute(pin_num_this - 2), Compute(pin_num_this - 1));

  else if (pin_num_this == 4 || pin_num_this == 11)
    ret = nor_gate(Compute(pin_num_this + 1), Compute(pin_num_this + 2));

  else if (_pin[pin_num_this - 1].first != NULL)
    ret = _pin[pin_num_this - 1].first->Compute(_pin[pin_num_this - 1].second);

  _temp[pin_num_this - 1].first = ret;
  _temp[pin_num_this - 1].second = false;

  return (ret);
}

void			Comp4001::SetLink(size_t pin_num_this,
					  nts::IComponent& component,
					  size_t pin_num_target)
{
  if (pin_num_this > 13 || pin_num_this == 7)
    throw std::runtime_error("bad pin");
  if (pin_num_this == 3 || pin_num_this == 4 ||
      pin_num_this == 10 || pin_num_this == 11)
    return ;
  _pin[pin_num_this - 1] = std::make_pair(&component, pin_num_target);
}

void			Comp4001::Dump(void) const
{
  for (int i = 0; i != 13 ; ++i)
    std::cout << _temp[i].first << " ";
  std::cout << std::endl;
}

//
// Comp4069.cpp for  in /home/anthony/rendu/cpp_nanotekspice
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Sat Feb 18 16:20:20 2017 Anthony Jouvel
// Last update Wed Mar  1 14:24:41 2017 Linsey Benoît
//

# include "Comp4069.hpp"

nts::Tristate           Comp4069::Compute(size_t pin_num_this)
{
  nts::Tristate		ret = nts::UNDEFINED;

  if (pin_num_this > 13 || pin_num_this == 7)
    throw std::runtime_error("pin does not exist");

  if (_temp[pin_num_this - 1].second == true)
    return (_temp[pin_num_this - 1].first);

  _temp[pin_num_this - 1].second = true;

  if (pin_num_this == 2 || pin_num_this == 4 || pin_num_this == 6)
    ret = not_gate(Compute(pin_num_this - 1));

  if (pin_num_this == 8 || pin_num_this == 10 || pin_num_this == 12)
    ret = not_gate(Compute(pin_num_this + 1));

  else if (_pin[pin_num_this - 1].first != NULL)
    ret = _pin[pin_num_this - 1].first->Compute(_pin[pin_num_this - 1].second);

  _temp[pin_num_this - 1].first = ret;
  _temp[pin_num_this - 1].second = false;

  return (ret);
}

void			Comp4069::SetLink(size_t pin_num_this,
					  nts::IComponent& component,
					  size_t pin_num_target)
{
  if (pin_num_this > 13 || pin_num_this == 7 || pin_num_this == 0)
    throw std::runtime_error("bad pin");
  if ((pin_num_this % 2) == 0)
    return ;
  _pin[pin_num_this - 1] = std::make_pair(&component, pin_num_target);
}

void			Comp4069::Dump(void) const
{
  std::cout << "4069: ";
  for (int i = 0; i != 13 ; ++i)
    std::cout << _temp[i].first << " ";
  std::cout << std::endl;
}

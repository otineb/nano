//
// Comp4017.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Sat Mar  4 10:14:50 2017 Linsey Benoît
// Last update Sat Mar  4 17:05:05 2017 Anthony Jouvel
//

# include "Comp4017.hpp"

std::map<size_t, nts::Tristate (Comp4017::*)(Clock *, nts::Tristate)> Comp4017::computeOutput =
  {
    {3, &Comp4017::computeQ0},
    {2, &Comp4017::computeQ1},
    {4, &Comp4017::computeQ2},
    {7, &Comp4017::computeQ3},
    {10, &Comp4017::computeQ4},
    {1, &Comp4017::computeQ5},
    {5, &Comp4017::computeQ6},
    {6, &Comp4017::computeQ7},
    {9, &Comp4017::computeQ8},
    {11, &Comp4017::computeQ9},
    {12, &Comp4017::computeCarryOut}
  };

std::map<size_t, nts::Tristate (Comp4017::*)()> Comp4017::computeInput =
  {
    {13, &Comp4017::computeCP1},
    {14, &Comp4017::computeCP0},
    {15, &Comp4017::computeMR}
  };

nts::Tristate		Comp4017::computeQ0(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    {
      return (nts::TRUE);
    }
  if (cl->getNbChanges() % 10 == 0)
    {
      return (nts::TRUE);
    }
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ1(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 1)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ2(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 2)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ3(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 3)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ4(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 4)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ5(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 5)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ6(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 6)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ7(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 7)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ8(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 8)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeQ9(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::FALSE);
  if (cl->getNbChanges() % 10 == 9)
    return (nts::TRUE);
  return (nts::FALSE);
}

nts::Tristate		Comp4017::computeCarryOut(Clock *cl, nts::Tristate enable)
{
  if (!cl)
    return (nts::UNDEFINED);
  if (enable == nts::TRUE)
    return (nts::TRUE);
  if (cl->getNbChanges() % 10 > 4)
    return (nts::FALSE);
  return (nts::TRUE);
}

nts::Tristate		Comp4017::computeCP0()
{
  if (_pin[13].first == NULL)
    return (nts::UNDEFINED);
  return (_pin[13].first->Compute());
}

nts::Tristate		Comp4017::computeCP1()
{
  nts::Tristate		ret;

  if (_pin[12].first == NULL)
    return (nts::UNDEFINED);
  ret = _pin[12].first->Compute();
  if (ret == nts::TRUE)
    ((Clock *)_pin[12].first)->lowerCounter();
  return (ret);
}

nts::Tristate		Comp4017::computeMR()
{
  if (_pin[14].first == NULL)
    return (nts::UNDEFINED);
  return (_pin[14].first->Compute());
}

nts::Tristate		Comp4017::Compute(size_t pin_num_this)
{
  nts::Tristate		ret = nts::UNDEFINED;

  if (pin_num_this > 15 || pin_num_this == 8)
    throw std::runtime_error("pin does not exist");

  if (_temp[pin_num_this - 1].second == true)
    return (_temp[pin_num_this - 1].first);

  _temp[pin_num_this - 1].second = true;

  if (computeInput.find(pin_num_this) != computeInput.end())
    ret = (this->*computeInput[pin_num_this])();
  else
    {
      Clock		*cl = NULL;
      nts::Tristate	enableState = Compute(15);

      if (_pin[13].first)
	cl = (Clock *)_pin[13].first;
      if (enableState == nts::TRUE)
	cl->setNbChanges();
      if (computeOutput.find(pin_num_this) == computeOutput.end())
	throw std::runtime_error("bad pin");
      ret = (this->*computeOutput[pin_num_this])(cl, enableState);
    }

  _temp[pin_num_this - 1].first = ret;
  _temp[pin_num_this - 1].second = false;

  return (ret);
}

void			Comp4017::SetLink(size_t pin_num_this,
					  nts::IComponent& component,
					  size_t pin_num_target)
{
  if (pin_num_this > 15)
    throw std::runtime_error("bad pin");
  if (pin_num_this < 13)
    return ;
  _pin[pin_num_this - 1] = std::make_pair(&component, pin_num_target);
}

void			Comp4017::Dump(void) const
{
  for (int i = 0; i != 15 ; ++i)
    std::cout << _temp[i].first << " ";
  std::cout << std::endl;
}

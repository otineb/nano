//
// base_component.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Mon Feb 13 15:23:29 2017 Linsey Benoît
// Last update Sat Mar  4 19:09:11 2017 Linsey Benoît
//

# include "base_component.hpp"

nts::Tristate			Input::Compute(size_t pin_num_this)
{
  return (_state);
  (void)pin_num_this;
}

void				Input::SetLink(size_t pin_num_this,
					       nts::IComponent &component,
					       size_t pin_num_target)
{
  std::vector<std::string>	err;

  err.push_back("1");
  if (pin_num_this != 1)
    throw BadTokenError(BadTokenError::UNKNOWN,
			std::to_string(pin_num_this),
			err);
  _pin = std::make_pair(&component, pin_num_target);
}

void				Input::Dump(void) const
{
  if (_state != nts::UNDEFINED)
    std::cout << _state << std::endl;
  else
    std::cout << "U" << std::endl;
}

void				Input::setState(nts::Tristate state)
{
  if (state != _state)
    _state = state;
}

void				Clock::invertState()
{
  if (_state == nts::FALSE)
    _state = nts::TRUE;
  else
    _state = nts::FALSE;
  _hasChanged = true;
  if (_lowerIt == false)
    _counter += 1;
  else
    _lowerIt = false;
}

bool				Clock::hasChanged() const
{
  return (_hasChanged);
}

void				Clock::lowerCounter()
{
  _lowerIt = true;
}

unsigned short			Clock::getNbChanges()
{
  return (_counter);
}

void				Clock::setNbChanges(unsigned short nb)
{
  _counter = nb;
}

nts::Tristate			True::Compute(size_t pin_num_this)
{
  return (nts::TRUE);
  (void)pin_num_this;
}

void				True::SetLink(size_t pin_num_this,
					      nts::IComponent &component,
					      size_t pin_num_target)
{
  return ;
  (void)pin_num_this;
  (void)component;
  (void)pin_num_target;
}

void				True::Dump(void) const
{
  std::cout << "1" << std::endl;
}

nts::Tristate			False::Compute(size_t pin_num_this)
{
  return (nts::FALSE);
  (void)pin_num_this;
}

void				False::SetLink(size_t pin_num_this,
					       nts::IComponent &component,
					       size_t pin_num_target)
{
  return ;
  (void)pin_num_this;
  (void)component;
  (void)pin_num_target;
}

void				False::Dump(void) const
{
  std::cout << "0" << std::endl;
}

nts::Tristate			Output::Compute(size_t pin_num_this)
{
  if (pin_num_this > 1)
    throw std::runtime_error("bad pin");
  if (_temp.second == true)
    return (_temp.first);
  _temp.first = _state;
  _temp.second = true;
  if (_pin.first)
    _state = _pin.first->Compute(_pin.second);
  _temp.second = false;
  return (_state);
}

void				Output::SetLink(size_t pin_num_this,
						nts::IComponent &component,
						size_t pin_num_target)
{
  if (pin_num_this > 1)
    throw std::runtime_error("bad pin");
  _pin = std::make_pair(&component, pin_num_target);
}

void				Output::Dump(void) const
{
  if (_state != nts::UNDEFINED)
    std::cout << _state << std::endl;
  else
    std::cout << "U" << std::endl;
}

bool				Output::isLinked(void) const
{
  return (_pin.first != NULL);
}

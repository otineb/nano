//
// Comp4013.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
// 
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
// 
// Started on  Sat Feb 18 15:16:19 2017 Linsey Benoît
// Last update Fri Mar  3 15:07:55 2017 Linsey Benoît
//

# include "Comp4013.hpp"

static nts::Tristate	getOutputRet(size_t pin, nts::Tristate q)
{
  nts::Tristate		nq;

  if (q == nts::TRUE)
    nq = nts::FALSE;
  else if (q == nts::FALSE)
    nq = nts::TRUE;
  else
    nq = nts::UNDEFINED;
  if (pin % 2 == 0)
    return nq;
  return q;
}

nts::Tristate		Comp4013::computeOutput(size_t pin)
{
  nts::Tristate		r, s, cl, q;

  r = s = cl = q = nts::UNDEFINED;
  if (pin < 3)
    {
      r = Compute(4);
      s = Compute(6);
      if (r == nts::TRUE || s == nts::TRUE)
	{
	  if (r == s)
	    return (r);
	  q = s;
	  return (getOutputRet(pin, q));
	}
      if (_pin[2].first != NULL && ((cl = Compute(3)) == nts::FALSE ||
				    ((Clock *)_pin[2].first)->hasChanged() == false))
	{
	  if (((Clock *)_pin[2].first)->hasChanged() == false)
	    return (nts::UNDEFINED);
	  if (((Clock *)_pin[2].first)->hasChanged())
	    {
	      if (_wasSwap[0] == false)
		{
		  swap_state(&_temp[0].first, &_temp[1].first);
		  _wasSwap[0] = true;
		}
	      else
		_wasSwap[0] = false;
	    }
	  return (getOutputRet(pin, _temp[0].first));
	}
      return (getOutputRet(pin, Compute(5)));
    }
  r = Compute(10);
  s = Compute(8);
  if (r == nts::TRUE || s == nts::TRUE)
    {
      if (r == s)
	return (r);
      q = s;
      return (getOutputRet(pin, q));
    }
  if (_pin[10].first != NULL && ((cl = Compute(11)) == nts::FALSE ||
				((Clock *)_pin[10].first)->hasChanged() == false))
    {
      if (((Clock *)_pin[10].first)->hasChanged() == false)
	return (nts::UNDEFINED);
      if (((Clock *)_pin[10].first)->hasChanged())
	{
	  if (_wasSwap[1] == false)
	    {
	      swap_state(&_temp[0].first, &_temp[11].first);
	      _wasSwap[1] = true;
	    }
	  else
	    _wasSwap[1] = false;
	}
      return (getOutputRet(pin, _temp[12].first));
    }
  return (getOutputRet(pin, Compute(9)));
}

nts::Tristate		Comp4013::Compute(size_t pin_num_this)
{
  nts::Tristate		ret = nts::UNDEFINED;

  if (pin_num_this > 13 || pin_num_this == 7)
    throw std::runtime_error("pin does not exist");

  if (_temp[pin_num_this - 1].second == true)
    return (_temp[pin_num_this - 1].first);

  _temp[pin_num_this - 1].second = true;

  if (pin_num_this == 1 || pin_num_this == 2 ||
      pin_num_this == 12 || pin_num_this == 13)
    ret = computeOutput(pin_num_this);
  else if (_pin[pin_num_this - 1].first != NULL)
    ret = _pin[pin_num_this - 1].first->Compute(_pin[pin_num_this - 1].second);

  _temp[pin_num_this - 1].first = ret;
  _temp[pin_num_this - 1].second = false;

  return (ret);
}

void			Comp4013::SetLink(size_t pin_num_this,
					  nts::IComponent& component,
					  size_t pin_num_target)
{
  if (pin_num_this > 13 || pin_num_this == 7)
    throw std::runtime_error("bad pin");
  if (pin_num_this == 1 || pin_num_this == 2 ||
      pin_num_this == 12 || pin_num_this == 13)
    return ;
  _pin[pin_num_this - 1] = std::make_pair(&component, pin_num_target);
}

void			Comp4013::Dump(void) const
{
  for (int i = 0; i != 13 ; ++i)
    std::cout << _temp[i].first << " ";
  std::cout << std::endl;
}

//
// exception.cpp for  in /home/anthony/rendu/cpp_nanotekspice
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Mon Feb 13 16:46:50 2017 Anthony Jouvel
// Last update Sun Mar  5 16:06:57 2017 Anthony Jouvel
//

# include "exception.hpp"

Error::Error(int line, std::string const &message) throw()
{
  std::ostringstream	oss;

  oss << "Error line " << line << " : " << message;
  this->_message = oss.str();
}

const char		*Error::what() const throw()
{
  return (this->_message.c_str());
}

BadParamError::BadParamError(BadParamError::TokenType const& type,
			     std::string const& err) throw()
{
  std::ostringstream	oss;

  if (type == BadParamError::M_FILE)
    oss << "Usage: " << err << " configuration_file [input/clock=value]";
  else if (type == BadParamError::B_FILE)
    oss << err << ": No such file";
  else if (type == BadParamError::VALUE)
    oss << "input/clock '" << err << "' must have a value";
  else
    oss << err << " is not an existing input/clock";
  _msg = oss.str();
  _type = type;
}

void			BadParamError::checkFile(char *fileName) throw()
{
  if (_type == BadParamError::B_FILE)
    _msg = std::string(fileName) + _msg;
}

const char		*BadParamError::what() const throw()
{
  return (_msg.c_str());
}

BadTokenError::BadTokenError(BadTokenError::TokenType const& type,
			     std::string token,
			     std::vector<std::string> tokens) throw()
{
  std::ostringstream	oss;

  if (type == BadTokenError::UNKNOWN)
    {
      oss << "unknown token: " << token << std::endl;
      if (tokens.size() > 1)
	{
	  oss << "expected one of the following: [";
	  for (const auto& it : tokens)
	    oss << it << ", ";
	  oss << "]";
	}
      else
	oss << "expected " << tokens.front();
    }
  else if (type == BadTokenError::MISSING)
    oss << "missing token near " << token;
  else if (type == BadTokenError::DUPLICATE)
    oss << token << " already exists and souldn't be duplicated";
  _msg = oss.str();
}

const char		*BadTokenError::what() const throw()
{
  return (_msg.c_str());
}

//
// Comp4081.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
// 
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
// 
// Started on  Fri Feb 17 18:15:27 2017 Linsey Benoît
// Last update Wed Mar  1 14:59:29 2017 Linsey Benoît
//

# include "Comp4081.hpp"

nts::Tristate		Comp4081::Compute(size_t pin_num_this)
{
  nts::Tristate		ret = nts::UNDEFINED;

  if (pin_num_this > 13 || pin_num_this == 7)
    throw std::runtime_error("pin does not exist");

  if (_temp[pin_num_this - 1].second == true)
    return (_temp[pin_num_this - 1].first);

  _temp[pin_num_this - 1].second = true;

  if (pin_num_this == 3 || pin_num_this == 10)
    ret = and_gate(Compute(pin_num_this - 2), Compute(pin_num_this - 1));

  else if (pin_num_this == 4 || pin_num_this == 11)
    ret = and_gate(Compute(pin_num_this + 1), Compute(pin_num_this + 2));

  else if (_pin[pin_num_this - 1].first != NULL)
    ret = _pin[pin_num_this - 1].first->Compute(_pin[pin_num_this - 1].second);

  _temp[pin_num_this - 1].first = ret;
  _temp[pin_num_this - 1].second = false;

  return (ret);
}

void			Comp4081::SetLink(size_t pin_num_this,
					  nts::IComponent& component,
					  size_t pin_num_target)
{
  if (pin_num_this > 13 || pin_num_this == 7)
    throw std::runtime_error("bad pin");
  if (pin_num_this == 3 || pin_num_this == 4 ||
      pin_num_this == 10 || pin_num_this == 11)
    return ;
  _pin[pin_num_this - 1] = std::make_pair(&component, pin_num_target);
}

void			Comp4081::Dump(void) const
{
  for (int i = 0; i != 13 ; ++i)
    std::cout << _temp[i].first << " ";
  std::cout << std::endl;
}

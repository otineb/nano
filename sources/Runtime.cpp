//
// Runtime.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Thu Feb 16 13:21:59 2017 Linsey Benoît
// Last update Sun Mar  5 15:13:35 2017 Linsey Benoît
//

# include "Runtime.hpp"
# include "exceptionUserInput.hpp"

bool Runtime::mustLeave = false;

const std::array<std::pair<const char *, void (Runtime::*)(void)>,
		 6> Runtime::userInput =
  {
    std::make_pair("exit", &Runtime::exit),
    std::make_pair("display", &Runtime::display),
    std::make_pair("simulate", &Runtime::simulate),
    std::make_pair("loop", &Runtime::loop),
    std::make_pair("dump", &Runtime::dump),
    std::make_pair("help", &Runtime::help),
  };

void			signalHandler(int sig)
{
  Runtime::mustLeave = true;
  (void)sig;
}

void			Runtime::help(void)
{
  std::cout << "• exit : It closes the program." << std::endl <<
    "• display : It prints on stdout the value of all outputs sorted by name." << std::endl <<
    "• input=value : It changes the value of an input. (Not a clock)" << std::endl <<
    "• simulate : It launches a simulation." << std::endl <<
    "• loop : Simulate without prompting again until SIGINT." << std::endl <<
    "• dump : Call the Dump method of every IComponent." << std::endl;
}

void			Runtime::exit(void)
{
  Runtime::mustLeave = true;
}

void			Runtime::display(void)
{
  for (std::pair<std::string, Output *> &it : _outputs)
    {
      std::cout << it.first << "=";
      it.second->Dump();
    }
}

static std::string	firstArrElement(std::pair<const char *, void (Runtime::*)(void)> const& p)
{
  return (std::string(p.first));
}

void							Runtime::changeInput(const std::string& param)
{
  std::map<std::string, nts::IComponent *>::iterator	comp;
  std::string						name;
  std::string						value;

  name = param.substr(0, param.find("="));
  value = param.substr(param.find("=") + 1);
  if ((comp = _components.find(name)) == _components.end() ||
      std::find(_inputs.begin(), _inputs.end(), name) == _inputs.end())
    throw BadTokenError(BadTokenError::UNKNOWN, name, _inputs);
  ((Input *)comp->second)->setState(getState(value));
}

void					Runtime::simulate(void)
{
  for (std::pair<std::string, Output *>& it : _outputs)
    it.second->Compute();
  for (Clock *cl : _clocks)
    cl->invertState();
}

void					Runtime::loop(void)
{
  while (15)
    {
      simulate();
      if (Runtime::mustLeave)
	{
	  Runtime::mustLeave = false;
	  break;
	}
    }
}

void					Runtime::dump(void)
{
  for (std::pair<std::string, nts::IComponent *> it : _components)
    {
      std::cout << it.first << ":" << std::endl;
      it.second->Dump();
    }
}

void					Runtime::run(void)
{
  std::string				user_input;
  bool					wasCalled;
  std::vector<std::string>		err;

  while (27)
    {
      if (Runtime::mustLeave)
	break ;
      std::cout << "> ";
      wasCalled = false;
      if (!std::getline(std::cin, user_input))
	break;
      for (auto it = Runtime::userInput.begin(); it != Runtime::userInput.end(); ++it)
	{
	  if (it->first == user_input)
	    {
	      (this->*(it->second))();
	      wasCalled = true;
	    }
	}
      if (!wasCalled)
	{
	  try
	    {
	      if (user_input.find("=") != std::string::npos)
		changeInput(user_input);
	      else
		{
		  std::transform(Runtime::userInput.begin(),
				 Runtime::userInput.end(),
				 std::back_inserter(err),
				 firstArrElement);
		  throw BadTokenError(BadTokenError::UNKNOWN, user_input, err);
		}
	    }
	  catch (const BadTokenError& e)
	    {
	      std::cout << e.what() << std::endl;
	    }
	}
    }
}

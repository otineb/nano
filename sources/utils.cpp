//
// utils.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Tue Feb 14 11:58:06 2017 Linsey Benoît
// Last update Sat Mar  4 17:14:00 2017 Anthony Jouvel
//

# include "utils.hpp"
# include "exception.hpp"

nts::Tristate		getState(const std::string& value)
{
  if (value.length() != 1)
    throw std::runtime_error("a value is only 1 or 0");
  if (value == "0")
    return (nts::FALSE);
  else if (value == "1")
    return (nts::TRUE);
  else
    throw std::runtime_error("a value is only 1 or 0");
}

void			swap_state(nts::Tristate *a, nts::Tristate *b)
{
  nts::Tristate		temp;

  temp = *a;
  *a = *b;
  *b = temp;
}

nts::Tristate		and_gate(const nts::Tristate& a, const nts::Tristate& b)
{
  if (a == nts::TRUE && b == nts::TRUE)
    return (nts::TRUE);
  else if (a == nts::FALSE || b == nts::FALSE)
    return (nts::FALSE);
  return (nts::UNDEFINED);
}

nts::Tristate		or_gate(const nts::Tristate& a, const nts::Tristate& b)
{
  if (a == nts::TRUE || b == nts::TRUE)
    return (nts::TRUE);
  else if (a == nts::FALSE && b == nts::FALSE)
    return (nts::FALSE);
  return (nts::UNDEFINED);
}

nts::Tristate		not_gate(const nts::Tristate& a)
{
  if (a == nts::FALSE)
    return (nts::TRUE);
  else if (a == nts::TRUE)
    return (nts::FALSE);
  return (nts::UNDEFINED);
}

nts::Tristate		nand_gate(const nts::Tristate& a, const nts::Tristate& b)
{
  if (a == nts::FALSE || b == nts::FALSE)
    return (nts::TRUE);
  else if (a == nts::TRUE && b == nts::TRUE)
    return (nts::FALSE);
  return (nts::UNDEFINED);
}

nts::Tristate		nor_gate(const nts::Tristate& a, const nts::Tristate& b)
{
  if (a == nts::TRUE || b == nts::TRUE)
    return (nts::FALSE);
  else if (a == nts::FALSE && b == nts::FALSE)
    return (nts::TRUE);
  return (nts::UNDEFINED);
}

nts::Tristate		xor_gate(const nts::Tristate& a, const nts::Tristate& b)
{
  if (a == nts::UNDEFINED || b == nts::UNDEFINED)
    return (nts::UNDEFINED);
  if (a == b)
    return (nts::FALSE);
  return (nts::TRUE);
}

nts::Tristate		xnor_gate(const nts::Tristate& a, const nts::Tristate& b)
{
  if (a == nts::UNDEFINED || b == nts::UNDEFINED)
    return (nts::UNDEFINED);
  if (a == b)
    return (nts::TRUE);
  return (nts::FALSE);
}

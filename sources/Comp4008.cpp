//
// Comp4008.cpp for  in /home/anthony/rendu/cpp/cpp_nanotekspice/sources
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Mon Feb 27 18:17:46 2017 Anthony Jouvel
// Last update Sat Mar  4 16:56:15 2017 Anthony Jouvel
//

# include "Comp4008.hpp"

std::map<size_t, nts::Tristate (Comp4008::*)()> Comp4008::computeOutput =
  {
    {10, &Comp4008::Compute10},
    {11, &Comp4008::Compute11},
    {12, &Comp4008::Compute12},
    {13, &Comp4008::Compute13},
    {14, &Comp4008::Compute14}
  };

nts::Tristate		Comp4008::Compute10(void)
{
  return (xor_gate(xor_gate(Compute(6), Compute(7)), Compute(9)));
}

nts::Tristate		Comp4008::Compute11(void)
{
  return (xor_gate(xor_gate(Compute(4), Compute(5)), getCarryOut(6, 7, 9)));
}

nts::Tristate		Comp4008::Compute12(void)
{
  return (xor_gate(xor_gate(Compute(2), Compute(3)), getCarryOut(4, 5, 10)));
}

nts::Tristate		Comp4008::Compute13(void)
{
  return (xor_gate(xor_gate(Compute(1), Compute(15)), getCarryOut(2, 3, 11)));
}

nts::Tristate		Comp4008::Compute14(void)
{
  return (getCarryOut(1, 15, 12));
}

nts::Tristate		Comp4008::getCarryOut(size_t const& a,
					      size_t const& b,
					      size_t const& cin)
{
  nts::Tristate		ret;

  if (a == 1)
    ret = or_gate(and_gate(Compute(a), Compute(b)),
		  and_gate(or_gate(Compute(a), Compute(b)),
			   getCarryOut(2, 3, 11)));

  else if (cin != 9)
    ret = or_gate(and_gate(Compute(a), Compute(b)),
		  and_gate(or_gate(Compute(a), Compute(b)),
			   getCarryOut(a + 2, b + 2, cin - 1)));

  else
    ret = or_gate(and_gate(Compute(a), Compute(b)),
		  and_gate(or_gate(Compute(a), Compute(b)), Compute(cin)));

  return (ret);
}

nts::Tristate		Comp4008::Compute(size_t pin_num_this)
{
  nts::Tristate		ret = nts::UNDEFINED;

  if  (pin_num_this > 15 || pin_num_this == 8)
    throw std::runtime_error("pin does not exist");

  if (_temp[pin_num_this - 1].second == true)
    return (_temp[pin_num_this - 1].first);

  _temp[pin_num_this - 1].second = true;

  if (computeOutput.find(pin_num_this) == computeOutput.end())
    {
      if (_pin[pin_num_this - 1].first != NULL)
	ret = _pin[pin_num_this - 1].first->Compute(_pin[pin_num_this - 1].second);
    }
  else
    ret = (this->*computeOutput[pin_num_this])();

  _temp[pin_num_this - 1].first = ret;
  _temp[pin_num_this - 1].second = false;

  return (ret);
}

void			Comp4008::SetLink(size_t pin_num_this,
					  nts::IComponent& component,
					  size_t pin_num_target)
{
  if (pin_num_this > 15 || pin_num_this == 8)
    throw std::runtime_error("bad pin");
  if (pin_num_this != 15 && pin_num_this >= 10)
    return ;
  _pin[pin_num_this - 1] = std::make_pair(&component, pin_num_target);
}

void			Comp4008::Dump(void) const
{
  std::cout << "4008: ";
  for (int i = 0; i != 15 ; ++i)
    std::cout << _temp[i].first << " ";
  std::cout << std::endl;
}

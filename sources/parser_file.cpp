//
// parser_file.cpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Mon Feb  6 18:03:01 2017 Linsey Benoît
// Last update Sun Mar  5 15:55:24 2017 Anthony Jouvel
//

#include "base_component.hpp"
#include "Comp4071.hpp"
#include "Comp4081.hpp"
#include "Comp4001.hpp"
#include "Comp4011.hpp"
#include "Comp4013.hpp"
#include "Comp4069.hpp"
#include "Comp4030.hpp"
#include "Comp4008.hpp"
#include "Comp4017.hpp"
#include "parser_file.hpp"
#include "exception.hpp"

const std::array<std::pair<const char *, nts::IComponent *(ParserFile::*)(const std::string&)>,
		 14> ParserFile::compoCreator =
  {
    std::make_pair("clock", &ParserFile::createClock),
    std::make_pair("true", &ParserFile::createTrue),
    std::make_pair("false", &ParserFile::createFalse),
    std::make_pair("input", &ParserFile::createInput),
    std::make_pair("output", &ParserFile::createOutput),
    std::make_pair("4001", &ParserFile::create4001),
    std::make_pair("4008", &ParserFile::create4008),
    std::make_pair("4011", &ParserFile::create4011),
    std::make_pair("4013", &ParserFile::create4013),
    std::make_pair("4017", &ParserFile::create4017),
    std::make_pair("4030", &ParserFile::create4030),
    std::make_pair("4069", &ParserFile::create4069),
    std::make_pair("4071", &ParserFile::create4071),
    std::make_pair("4081", &ParserFile::create4081)
  };

std::vector<std::pair<std::string, Output *>>	&ParserFile::getOut(void)
{
  return (_outputs);
}

std::vector<Clock *>				&ParserFile::getCl(void)
{
  return (_clocks);
}

std::map<std::string, nts::IComponent *>	&ParserFile::getComp(void)
{
  return (_components);
}

std::vector<std::string>			&ParserFile::getIn(void)
{
  return (_inputs);
}

void			ParserFile::feed(std::string const& input)
{
  size_t		ret;
  std::string		meat;

  if (input[0] == '\n' || input[0] == '#')
    return ;
  ret = input.find("#");
  if (ret != std::string::npos && ret != 0)
    meat = input.substr(0, ret - 1);
  else if (ret != 0 && ret == std::string::npos)
    meat = input;
  if (std::all_of(meat.begin(),
		  meat.end(),
		  [](int c){return isspace(c);}))
    return ;
  _input += meat;
  _input += '\n';
}

nts::t_ast_node				*ParserFile::getNewline()
{
  nts::t_ast_node			*tmp;

  tmp = new nts::s_ast_node(NULL);
  tmp->type = nts::ASTNodeType::NEWLINE;
  tmp->lexeme = _input[_index];
  ++_index;
  return (tmp);
}

void					ParserFile::skipSpace()
{
  while (_input[_index] == ' ' || _input[_index] == '\t')
    ++_index;
}

nts::t_ast_node				*ParserFile::getString()
{
  nts::t_ast_node			*tmp = new nts::t_ast_node(NULL);
  unsigned int				i = 0;

  this->skipSpace();
  while (_input[_index] && _input[_index] != ':' && _input[_index] != '.'
	 && _input[_index] != '\n' && _input[_index] != ' ' && _input[_index] != '\t')
    {
      ++_index;
      ++i;
    }
  tmp->type = nts::ASTNodeType::STRING;
  if (i != 0)
    tmp->lexeme = _input.substr(_index - i, i);
  if (_input[_index] != '\n')
    ++_index;
  this->skipSpace();
  return (tmp);
}

nts::t_ast_node				*ParserFile::getLinkEnd()
{
  nts::t_ast_node			*tmp = new nts::t_ast_node(this->getLink()->children);

  tmp->type = nts::ASTNodeType::LINK_END;
  return (tmp);
}

nts::t_ast_node				*ParserFile::getLink()
{
  nts::t_ast_node			*tmp;
  std::vector<nts::t_ast_node*>		*children = new std::vector<nts::t_ast_node*>();

  children->push_back(this->getString());
  if (_input[_index] == '\n')
    children->push_back(this->getNewline());
  children->push_back(this->getString());
  if (_input[_index] == '\n')
    children->push_back(this->getNewline());
  tmp = new nts::t_ast_node(children);
  tmp->type = nts::ASTNodeType::LINK;
  return (tmp);
}

nts::t_ast_node				*ParserFile::getComponent()
{
  nts::t_ast_node			*tmp;
  std::vector<nts::t_ast_node*>		*children = new std::vector<nts::t_ast_node*>();

  children->push_back(this->getString());
  if (_input[_index] == '\n')
    children->push_back(this->getNewline());
  children->push_back(this->getString());
  if (_input[_index] == '\n')
    children->push_back(this->getNewline());
  tmp = new nts::t_ast_node(children);
  tmp->type = nts::ASTNodeType::COMPONENT;
  return (tmp);
}

nts::t_ast_node				*ParserFile::getSection()
{
  nts::t_ast_node			*section = new nts::t_ast_node(NULL);
  std::vector<nts::t_ast_node*>		*children = new std::vector<nts::t_ast_node*>();
  size_t				ret;

  section->type = nts::ASTNodeType::SECTION;
  ret = _input.find(":", _index);
  section->lexeme = _input.substr(_index + 1, (ret - _index) - 1);
  _index = ret + 1;
  while (_input[_index] && _input[_index] != '.')
    {
      if (_input[_index] == '\n')
	children->push_back(getNewline());
      else
	{
	  if ((ret = (_input.substr(_index, _input.find("\n", _index) - _index).find(":"))) != std::string::npos)
	    {
	      children->push_back(getLink());
	      children->push_back(getLinkEnd());
	    }
	  else
	    children->push_back(getComponent());
	}
    }
  section->children = children;
  return (section);
}

/*
** lexeur
*/

nts::t_ast_node				*ParserFile::createTree()
{
  nts::t_ast_node			*tmp;
  std::vector<nts::t_ast_node*>		*children = new std::vector<nts::t_ast_node*>();

  while (_input[_index])
    {
      if (_input[_index] == '.')
	children->push_back(this->getSection());
      else if (_input[_index] == '\n')
	children->push_back(this->getNewline());
      else
	++_index;
    }
  tmp = new nts::t_ast_node(children);
  tmp->type = nts::ASTNodeType::DEFAULT;
  return (tmp);
}

/*
** affiche le type d'un noeud --> debug
*/

void aff_type(nts::ASTNodeType type)
{
  if (type == nts::ASTNodeType::DEFAULT)
    std::cout << "| racine | \n";
  else if (type == nts::ASTNodeType::SECTION)
    std::cout << "| section | \n";
  else if (type == nts::ASTNodeType::COMPONENT)
    std::cout << "| component | \n";
  else if (type == nts::ASTNodeType::LINK)
    std::cout << "| link | \n";
  else if (type == nts::ASTNodeType::LINK_END)
    std::cout << "| link-end | \n";
  else if (type == nts::ASTNodeType::NEWLINE)
    std::cout << "| newline | \n";
}

void aff_tree(nts::t_ast_node& root) {
  aff_type(root.type);
  std::cout << root.lexeme << std::endl;
  if (root.children == NULL)
    {
      return ;
    }
  if (root.children)
    std::cout << "{{\n";
  for (nts::t_ast_node *node : *(root.children))
    {
      aff_tree(*node);
    }
  if (root.children)
    std::cout << "\n}}";
}

nts::Tristate			ParserFile::argIntegrity(const std::string& value)
{
  int				i = 0;
  std::string			tmp;

  if (!_args)
    throw BadParamError(BadParamError::VALUE, value);
  while (_args[i])
    {
      tmp = std::string(_args[i]);
      if (tmp.substr(0, value.length()) == value)
	break;
      ++i;
    }
  if (!_args[i])
    throw BadParamError(BadParamError::VALUE, value);
  return (getState(tmp.substr(value.length() + 1)));
}

nts::IComponent			*ParserFile::createInput(const std::string& value)
{
  Input				*input = new Input();

  _inputs.push_back(std::string(value));
  input->setState(argIntegrity(value));
  return (input);
}

nts::IComponent			*ParserFile::createOutput(const std::string& value)
{
  Output			*ret = new Output();

  _outputs.push_back(std::make_pair(value, ret));
  return (ret);
}

nts::IComponent			*ParserFile::createClock(const std::string& value)
{
  Clock				*cl = new Clock();

  _inputs.push_back(std::string(value));
  cl->setState(argIntegrity(value));
  _clocks.push_back(cl);
  return (cl);
}

nts::IComponent			*ParserFile::createTrue(const std::string& value)
{
  return (new True());
  (void)value;
}

nts::IComponent			*ParserFile::createFalse(const std::string& value)
{
  return (new False());
  (void)value;
}

nts::IComponent			*ParserFile::create4008(const std::string& value)
{
  return (new Comp4008());
  (void)value;
}

nts::IComponent			*ParserFile::create4030(const std::string& value)
{
  return (new Comp4030());
  (void)value;
}

nts::IComponent			*ParserFile::create4069(const std::string& value)
{
  return (new Comp4069());
  (void)value;
}

nts::IComponent			*ParserFile::create4071(const std::string& value)
{
  return (new Comp4071());
  (void)value;
}

nts::IComponent			*ParserFile::create4081(const std::string& value)
{
  return (new Comp4081());
  (void)value;
}

nts::IComponent			*ParserFile::create4001(const std::string& value)
{
  return (new Comp4001());
  (void)value;
}

nts::IComponent			*ParserFile::create4011(const std::string& value)
{
  return (new Comp4011());
  (void)value;
}

nts::IComponent			*ParserFile::create4013(const std::string& value)
{
  return (new Comp4013());
  (void)value;
}

nts::IComponent			*ParserFile::create4017(const std::string& value)
{
  return (new Comp4017());
  (void)value;
}

std::string		firstElement(std::pair<const char *,
				     nts::IComponent *(ParserFile::*)(const std::string&)> const& p)
{
  return (std::string(p.first));
}

nts::IComponent				*ParserFile::createComponent(const std::string &type, const std::string &value)
{
  auto					it = ParserFile::compoCreator.begin();
  std::vector<std::string>		err;

  while (it != ParserFile::compoCreator.end())
    {
      if ((*it).first == type)
	break;
      ++it;
    }
  if (it == ParserFile::compoCreator.end())
    {
      std::transform(compoCreator.begin(),
		     compoCreator.end(),
		     std::back_inserter(err),
		     firstElement);
      throw BadTokenError(BadTokenError::UNKNOWN, type, err);
    }
  return ((this->*(it->second))(value));
}

void					ParserFile::verifyComponent(nts::t_ast_node *comp)
{
  std::vector<std::string>		err;

  if (!comp->children)
    throw std::runtime_error(UNKNOWN_ERROR);
  if (comp->children->size() != 3)
    throw BadTokenError(BadTokenError::MISSING, comp->children->front()->lexeme);
  if (_components.find((*comp->children)[1]->lexeme) != _components.end())
    {
      std::transform(compoCreator.begin(), compoCreator.end(), std::back_inserter(err), firstElement);
      throw BadTokenError(BadTokenError::DUPLICATE, (*comp->children)[1]->lexeme, err);
    }
  _components[(*comp->children)[1]->lexeme] = createComponent((*comp->children)[0]->lexeme, (*comp->children)[1]->lexeme);
}

void				ParserFile::verifyChipsets(nts::s_ast_node *chipsets)
{
  std::vector<std::string>	err;

  err.push_back("chipsets");
  if (chipsets->type != nts::ASTNodeType::SECTION || chipsets->lexeme != "chipsets")
    {
      std::cout << "ERR:" << chipsets->lexeme;
      throw BadTokenError(BadTokenError::UNKNOWN, chipsets->lexeme, err);
    }
  for (nts::t_ast_node *node : *(chipsets->children))
    {
      if (node->type != nts::ASTNodeType::COMPONENT && node->type != nts::ASTNodeType::NEWLINE)
	throw BadTokenError(BadTokenError::MISSING, node->lexeme);
      if (node->type == nts::ASTNodeType::COMPONENT)
	verifyComponent(node);
    }
}

void							ParserFile::verifyLink(nts::t_ast_node *linked, nts::t_ast_node *linker)
{
  std::map<std::string, nts::IComponent*>::iterator	comp1, comp2;
  std::vector<std::string>				err;

  for (const auto& it : _components)
    err.push_back(it.first);
  if ((comp1 = (_components.find((*linked->children)[0]->lexeme))) == _components.end())
    throw BadTokenError(BadTokenError::UNKNOWN, (*linked->children)[0]->lexeme, err);
  if ((comp2 = (_components.find((*linker->children)[0]->lexeme))) == _components.end())
    throw BadTokenError(BadTokenError::UNKNOWN, (*linker->children)[0]->lexeme, err);

  comp2->second->SetLink(std::stoul((*linker->children)[1]->lexeme),
			 *comp1->second,
			 std::stoul((*linked->children)[1]->lexeme));

  comp1->second->SetLink(std::stoul((*linked->children)[1]->lexeme),
			 *comp2->second,
			 std::stoul((*linker->children)[1]->lexeme));
}

void					ParserFile::verifyLinks(nts::t_ast_node *links)
{
  std::vector<std::string>		err;

  err.push_back("links");
  if (links->type != nts::ASTNodeType::SECTION || links->lexeme != "links")
    throw BadTokenError(BadTokenError::UNKNOWN,
			links->lexeme,
			err);
  auto it = links->children->begin();
  while (it != links->children->end())
    {
      if ((*it)->type != nts::ASTNodeType::NEWLINE)
	{
	  if ((*it)->type != nts::ASTNodeType::LINK)
	    throw BadTokenError(BadTokenError::MISSING,	(*it)->lexeme);
	  if ((*(it + 1))->type != nts::ASTNodeType::LINK_END)
	    throw BadTokenError(BadTokenError::MISSING, (*(it + 1))->lexeme);
	  verifyLink(*it, *(it + 1));
	  it += 2;
	}
      else
	++it;
    }
}

/*
** parser
*/

void							ParserFile::parseTree(nts::t_ast_node& root)
{
  //  aff_tree(root);
  if (!root.children || root.children->size() == 0)
    throw BadParamError(BadParamError::B_FILE, "");
  if (root.children->size() < 2)
    throw BadTokenError(BadTokenError::MISSING, root.children->front()->lexeme);

  unsigned int i = 0;
  for (const auto& e : *(root.children))
    {
      if (e->type != nts::ASTNodeType::NEWLINE)
	break;
      ++i;
    }
  if (i + 1 >= root.children->size())
    throw std::runtime_error(UNKNOWN_ERROR);
  this->verifyChipsets((*root.children)[i]);

  this->verifyLinks((*root.children)[i + 1]);

  if (!std::all_of(_outputs.begin(),
		   _outputs.end(),
		   [](std::pair<std::string, Output*> e){return e.second->isLinked();}))
    throw std::logic_error("Not every output is linked");

  std::vector<std::string> duplicateCheck;
  for (int i = 1; _args[i] != NULL; ++i)
    {
      std::string arg = std::string(_args[i]);
      size_t n = arg.find("=");
      if (n == std::string::npos)
	throw BadParamError(BadParamError::NOT_EXISTING, arg);
      std::string name = arg.substr(0, n);
      if (std::find(_inputs.begin(), _inputs.end(), name) == _inputs.end())
	throw BadParamError(BadParamError::NOT_EXISTING, name);
      duplicateCheck.push_back(name);
    }
  std::sort(duplicateCheck.begin(), duplicateCheck.end());
  if (_args[1])
    {
      for (size_t i = 0 ; i != duplicateCheck.size() - 1; ++i)
	{
	  if (duplicateCheck[i] == duplicateCheck[i + 1])
	    throw BadTokenError(BadTokenError::DUPLICATE, duplicateCheck[i]);
	}
    }
  std::sort(_outputs.begin(), _outputs.end());
}

##
## Makefile for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
##
## Made by Linsey Benoît
## Login   <benoit.linsey@epitech.eu>
##
## Started on  Thu Feb  9 14:18:45 2017 Linsey Benoît
## Last update Sun Mar  5 14:25:20 2017 Linsey Benoît
##

SRCS            =	sources/parser_file.cpp \
			sources/exception.cpp \
			sources/exceptionUserInput.cpp \
			sources/base_component.cpp \
			sources/Comp4071.cpp \
			sources/Comp4081.cpp \
			sources/Comp4001.cpp \
			sources/Comp4011.cpp \
			sources/Comp4013.cpp \
			sources/Comp4069.cpp \
			sources/Comp4008.cpp \
			sources/Comp4030.cpp \
			sources/Comp4017.cpp \
			sources/utils.cpp \
			sources/Runtime.cpp

MAIN		=	sources/main.cpp

LIB		=	libnanotekspice.a

NAME            =	nanotekspice

CPP		=	g++

RM              =	rm -f

OBJS            =	$(SRCS:.cpp=.o)

MAIN_O		=	$(MAIN:.cpp=.o)

CXXFLAGS	+=	-Werror -Wall -Wextra -std=c++11
CXXFLAGS	+=	-I includes/

all:		$(NAME)

$(NAME):	$(OBJS) $(MAIN_O)
		ar rc $(LIB) $(OBJS)
		ranlib $(LIB)
		$(CPP) $(MAIN_O) $(LIB) -o $(NAME)

clean:
		$(RM) $(OBJS) $(MAIN_O) $(LIB)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re

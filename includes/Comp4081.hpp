//
// Comp4081.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Fri Feb 17 13:50:46 2017 Linsey Benoît
// Last update Sat Mar  4 19:01:36 2017 Anthony Jouvel
//

# include "parser_file.hpp"

class Comp4081 : public nts::IComponent
{
protected:
  std::array<std::pair<nts::Tristate, bool>, 13>	_temp;
  std::array<std::pair<IComponent *, int>, 13>		_pin;

public:
  Comp4081()
  {
    _temp.fill(std::make_pair(nts::UNDEFINED, false));
    for (int i = 0; i != 13; ++i)
      {
	_pin[i].first = NULL;
      }
  };
  virtual			~Comp4081() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  virtual void			Dump(void) const;
};

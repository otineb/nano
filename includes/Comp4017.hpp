//
// Comp4017.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Sat Mar  4 10:01:31 2017 Linsey Benoît
// Last update Sat Mar  4 19:00:37 2017 Anthony Jouvel
//

# include "parser_file.hpp"

class Comp4017 : public nts::IComponent
{
protected:
  std::array<std::pair<nts::Tristate, bool>, 15>	_temp;
  std::array<std::pair<IComponent *, int>, 15>		_pin;

public:
  Comp4017()
  {
    _temp.fill(std::make_pair(nts::UNDEFINED, false));
    for (int i = 0; i != 15; ++i)
      {
	_pin[i].first = NULL;
      }
  };
  virtual			~Comp4017() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  virtual void			Dump(void) const;


protected:
  nts::Tristate		computeCP0();
  nts::Tristate		computeCP1();
  nts::Tristate		computeMR();
  nts::Tristate		computeQ0(Clock *, nts::Tristate);
  nts::Tristate		computeQ1(Clock *, nts::Tristate);
  nts::Tristate		computeQ2(Clock *, nts::Tristate);
  nts::Tristate		computeQ3(Clock *, nts::Tristate);
  nts::Tristate		computeQ4(Clock *, nts::Tristate);
  nts::Tristate		computeQ5(Clock *, nts::Tristate);
  nts::Tristate		computeQ6(Clock *, nts::Tristate);
  nts::Tristate		computeQ7(Clock *, nts::Tristate);
  nts::Tristate		computeQ8(Clock *, nts::Tristate);
  nts::Tristate		computeQ9(Clock *, nts::Tristate);
  nts::Tristate		computeCarryOut(Clock *, nts::Tristate);


  static std::map<size_t, nts::Tristate (Comp4017::*)(Clock *, nts::Tristate)> computeOutput;
  static std::map<size_t, nts::Tristate (Comp4017::*)()> computeInput;
};

//
// Comp4013.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Sat Feb 18 14:33:02 2017 Linsey Benoît
// Last update Sat Mar  4 19:00:24 2017 Anthony Jouvel
//

# include "parser_file.hpp"

class Comp4013 : public nts::IComponent
{
protected:
  std::array<std::pair<nts::Tristate, bool>, 13>	_temp;
  std::array<std::pair<IComponent *, int>, 13>		_pin;
  std::array<bool, 2>					_wasSwap;

public:
  Comp4013()
  {
    _temp.fill(std::make_pair(nts::UNDEFINED, false));
    _wasSwap.fill(false);
    for (int i = 0; i != 13; ++i)
      {
	_pin[i].first = NULL;
      }
  };
  virtual			~Comp4013() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  nts::Tristate			computeOutput(size_t);
  virtual void			Dump(void) const;
};

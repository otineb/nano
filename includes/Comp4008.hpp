//
// Comp4008.hpp for  in /home/anthony/rendu/cpp/cpp_nanotekspice/sources
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Wed Mar  1 10:46:48 2017 Anthony Jouvel
// Last update Sat Mar  4 12:35:20 2017 Anthony Jouvel
//

# include "parser_file.hpp"

class Comp4008 : public nts::IComponent
{
protected:
  std::array<std::pair<nts::Tristate, bool>, 15>	_temp;
  std::array<std::pair<IComponent *, int>, 15>		_pin;

public:
  Comp4008()
  {
    _temp.fill(std::make_pair(nts::UNDEFINED, false));
    for (int i = 0; i != 15; ++i)
      {
	_pin[i].first = NULL;
      }
  };

  virtual			~Comp4008() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  nts::Tristate				getCarryOut(size_t const& a,
					    size_t const& b,
					    size_t const & cin);
  virtual void			Dump(void) const;

  static std::map<size_t, nts::Tristate (Comp4008::*)()>	computeOutput;

  nts::Tristate			Compute10(void);
  nts::Tristate			Compute11(void);
  nts::Tristate			Compute12(void);
  nts::Tristate			Compute13(void);
  nts::Tristate			Compute14(void);
};

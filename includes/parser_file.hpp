//
// parser_file.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Mon Feb  6 16:59:53 2017 Linsey Benoît
// Last update Sun Mar  5 15:10:46 2017 Linsey Benoît
//

#ifndef PARSER_FILE_HPP
# define PARSER_FILE_HPP

# include <algorithm>
# include <iostream>
# include <fstream>
# include <map>
# include <utility>
# include <stdexcept>
# include "base_component.hpp"
# include "IParser.hpp"
# include "IComponent.hpp"
# include "utils.hpp"

# define UNKNOWN_ERROR "Unknown error occured"

class Output;
class Clock;

class ParserFile : nts::IParser
{
protected:
  int						_index;
  std::string					_input;
  char						**_args;
  std::map<std::string, nts::IComponent *>	_components;
  std::vector<std::pair<std::string, Output *>>	_outputs;
  std::vector<Clock *>				_clocks;
  std::vector<std::string>			_inputs;

public:
  virtual void					feed(std::string const& input);
  virtual void					parseTree(nts::t_ast_node& root);
  virtual nts::t_ast_node			*createTree();
  std::vector<std::pair<std::string, Output *>>	&getOut(void);
  std::map<std::string, nts::IComponent *> 	&getComp(void);
  std::vector<Clock *>				&getCl(void);
  std::vector<std::string>			&getIn(void);

  virtual ~ParserFile() {}
private:
  nts::t_ast_node			*getComponent();
  nts::t_ast_node			*getLink();
  nts::t_ast_node			*getLinkEnd();
  nts::t_ast_node			*getSection();
  nts::t_ast_node			*getNewline();
  nts::t_ast_node			*getString();
  void					skipSpace();
  void					verifyChipsets(nts::s_ast_node *);
  void					verifyLinks(nts::s_ast_node *);
  void					verifyLink(nts::t_ast_node *, nts::t_ast_node *);
  void					verifyComponent(nts::t_ast_node *);

  nts::Tristate				argIntegrity(const std::string&);
  nts::IComponent			*createComponent(const std::string &type, const std::string &value);
  nts::IComponent			*createInput(const std::string&);
  nts::IComponent			*createOutput(const std::string&);
  nts::IComponent			*create4071(const std::string&);
  nts::IComponent			*create4081(const std::string&);
  nts::IComponent			*create4001(const std::string&);
  nts::IComponent			*create4011(const std::string&);
  nts::IComponent			*create4013(const std::string&);
  nts::IComponent			*create4069(const std::string&);
  nts::IComponent			*create4030(const std::string&);
  nts::IComponent			*create4008(const std::string&);
  nts::IComponent			*create4017(const std::string&);
  nts::IComponent			*createClock(const std::string&);
  nts::IComponent			*createTrue(const std::string&);
  nts::IComponent			*createFalse(const std::string&);

public:
  ParserFile(char **args) : _index(0), _input(std::string()), _args(args),
			    _components(std::map<std::string, nts::IComponent *>()),
			    _outputs(std::vector<std::pair<std::string, Output *>>()),
			    _clocks(std::vector<Clock *>()),
			    _inputs(std::vector<std::string>()){}

public:
  static const std::array<std::pair<const char *, nts::IComponent *(ParserFile::*)(const std::string&)>, 14> compoCreator;
};

# endif //PARSER_FILE_HPP

//
// Runtime.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Thu Feb 16 11:52:19 2017 Linsey Benoît
// Last update Sun Mar  5 15:06:19 2017 Linsey Benoît
//

#ifndef RUNTIME_HPP_
# define RUNTIME_HPP

# include <csignal>
# include "parser_file.hpp"
# include "IComponent.hpp"
# include "utils.hpp"

void	signalHandler(int);

class Runtime
{
protected:
  std::vector<std::pair<std::string, Output *>>&	_outputs;
  std::map<std::string, nts::IComponent *>&		_components;
  std::vector<Clock *>&					_clocks;
  std::vector<std::string>&				_inputs;

public:
  void							exit(void);
  void							display(void);
  void							changeInput(const std::string& param);
  void							simulate(void);
  void							loop(void);
  void							dump(void);
  void							run(void);
  void							help(void);

public:
  Runtime(std::vector<std::pair<std::string, Output *>>& o,
	  std::map<std::string, nts::IComponent *>& c,
	  std::vector<Clock *>& cl,
	  std::vector<std::string>& in) :
    _outputs(o), _components(c), _clocks(cl), _inputs(in)
  {
    signal(SIGINT, signalHandler);
    this->simulate();
    this->display();
  }
  ~Runtime() {}

  static bool mustLeave;
  static const std::array<std::pair<const char *, void (Runtime::*)(void)>, 6> userInput;
};

# endif //RUNTIME_HPP_

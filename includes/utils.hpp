//
// utils.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
//
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
//
// Started on  Wed Feb 15 14:20:46 2017 Linsey Benoît
// Last update Thu Mar  2 22:56:39 2017 Linsey Benoît
//

#ifndef UTILS_HPP_
# define UTILS_HPP_

# include "IComponent.hpp"
# include "exception.hpp"

nts::Tristate		getState(const std::string&);
nts::Tristate		or_gate(const nts::Tristate&, const nts::Tristate&);
nts::Tristate		and_gate(const nts::Tristate&, const nts::Tristate&);
nts::Tristate		xor_gate(const nts::Tristate&, const nts::Tristate&);
nts::Tristate		nand_gate(const nts::Tristate&, const nts::Tristate&);
nts::Tristate		nor_gate(const nts::Tristate&, const nts::Tristate&);
nts::Tristate		not_gate(const nts::Tristate&);
nts::Tristate		xnor_gate(const nts::Tristate&, const nts::Tristate&);
void			swap_state(nts::Tristate *a, nts::Tristate *b);

# endif //UTILS_HPP_

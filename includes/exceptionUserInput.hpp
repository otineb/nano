//
// exceptionUserInput.hpp for  in /home/anthony/rendu/cpp_nanotekspice
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Thu Feb 16 15:22:26 2017 Anthony Jouvel
// Last update Thu Feb 16 16:51:43 2017 Anthony Jouvel
//

#ifndef EXCEPTION_USER_INPUT_HPP_
# define EXCEPTION_USER_INPUT_HPP_

# include <iostream>
# include <sstream>
# include <exception>

class UserInputError : public std::exception
{
public:
  UserInputError(std::string const& wrongInput) throw();
  virtual ~UserInputError() throw(){}
  virtual const char *what() const throw();
private:
  std::string	_message;
};

# endif /* !EXCEPTION_USER_INPUT_HPP_ */

//
// base_component.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
// 
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
// 
// Started on  Mon Feb 13 10:15:01 2017 Linsey Benoît
// Last update Sat Mar  4 15:57:47 2017 Linsey Benoît
//

#ifndef BASE_COMPONENT_HPP_
# define BASE_COMPONENT_HPP_

# include "parser_file.hpp"
# include "utils.hpp"
# include "exception.hpp"

class Input : public nts::IComponent
{
protected:
  nts::Tristate				_state;
  std::pair<nts::Tristate, bool>	_temp;
  std::pair<IComponent *, int>		_pin;
  
public:
  Input() : _state(nts::UNDEFINED),
	    _temp({nts::UNDEFINED, false}),
	    _pin({NULL, -1}){};
  virtual			~Input() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  virtual void			Dump(void) const;
  void				setState(nts::Tristate);
};

class Clock : public Input
{
private:
  bool			_hasChanged;
  bool			_lowerIt;

public:
  Clock() : Input(), _hasChanged(false), _lowerIt(false), _counter(0) {}
  virtual ~Clock() {}
  void				invertState();
  bool				hasChanged() const;

  unsigned short		_counter;
  unsigned short		getNbChanges();
  void				setNbChanges(unsigned short nb=0);
  void				lowerCounter();
};

class True : public nts::IComponent
{
public:
  True() {}
  ~True() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  virtual void			Dump(void) const;
};

class False : public nts::IComponent
{
public:
  False() {}
  ~False() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  virtual void			Dump(void) const;
};

class Output : public nts::IComponent
{
protected:
  nts::Tristate				_state;
  std::pair<nts::Tristate, bool>	_temp;
  std::pair<IComponent *, int>		_pin;

public:
  Output() : _state(nts::UNDEFINED),
	     _temp(std::make_pair(nts::UNDEFINED, false)),
	     _pin({NULL, -1}) {};
  virtual			~Output() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  virtual void			Dump(void) const;
  bool				isLinked(void) const;
};
  
# endif //BASE_COMPONENT_HPP_

//
// Comp4069.hpp for  in /home/anthony/rendu/cpp_nanotekspice
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Sat Feb 18 16:16:32 2017 Anthony Jouvel
// Last update Wed Mar  1 14:16:58 2017 Linsey Benoît
//

# include "parser_file.hpp"

class Comp4069 : public nts::IComponent
{
protected:
  std::array<std::pair<nts::Tristate, bool>, 13>        _temp;
  std::array<std::pair<IComponent *, int>, 13>          _pin;

public:
  Comp4069()
  {
    _temp.fill(std::make_pair(nts::UNDEFINED, false));
    for (int i = 0; i != 13; ++i)
      {
	_pin[i].first = NULL;
      }
  };
  virtual			~Comp4069() {}
  virtual nts::Tristate		Compute(size_t pin_num_this = 1);
  virtual void			SetLink(size_t pin_num_this,
					nts::IComponent &component,
					size_t pin_num_target);
  virtual void			Dump(void) const;

};

//
// exception.hpp for  in /home/anthony/rendu/cpp_nanotekspice
//
// Made by Anthony Jouvel
// Login   <anthony.jouvel@epitech.eu>
//
// Started on  Mon Feb 13 15:30:36 2017 Anthony Jouvel
// Last update Sun Mar  5 16:03:20 2017 Anthony Jouvel
//

#ifndef EXCEPTION_HPP_
# define EXCEPTION_HPP_

# include <iostream>
# include <sstream>
# include <exception>
# include <vector>

class Error : public std::exception
{
public:
  Error(int line, std::string const& message) throw();
  virtual ~Error() throw(){}
  virtual const char *what() const throw();
  int const &getLine() const throw();
private:
  std::string	_message;
};

class BadParamError : public std::exception
{
public:

  enum TokenType
    {
      M_FILE,
      B_FILE,
      VALUE,
      NOT_EXISTING
    };

  BadParamError(BadParamError::TokenType const& type,
		std::string const& err) throw();
  virtual ~BadParamError() throw() {}
  virtual const char *what() const throw();
  void checkFile(char *) throw();
private:
  std::string			_msg;
  BadParamError::TokenType	_type;
};

class BadTokenError : public std::exception
{
public:
  enum TokenType
    {
      UNKNOWN,
      DUPLICATE,
      MISSING
    };

  BadTokenError(BadTokenError::TokenType const& type,
		std::string token="",
		std::vector<std::string> tokens=std::vector<std::string>()) throw();
  virtual ~BadTokenError() throw() {}
  virtual const char *what() const throw();
private:
  std::string _msg;
};

#endif /* !EXCEPTION_HPP_ */

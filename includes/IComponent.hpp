//
// IComponents.hpp for nanotekspice in /home/benito/rendu/C++/cpp_nanotekspice
// 
// Made by Linsey Benoît
// Login   <benoit.linsey@epitech.eu>
// 
// Started on  Sat Feb 11 14:42:41 2017 Linsey Benoît
// Last update Wed Feb 15 22:36:17 2017 Linsey Benoît
//

#ifndef __ICOMPONENT_HPP__
# define __ICOMPONENT_HPP__

#include <iostream>

namespace nts
{
  enum Tristate
    {
      UNDEFINED = (-true),
      TRUE = true,
      FALSE = false
    };
  
  class IComponent
  {
  public:
    /// Compute value of the precised pin
    virtual nts::Tristate Compute(size_t pin_num_this = 1) = 0;
    
    /// Useful to link IComponent together
    virtual void SetLink(size_t pin_num_this,
			 nts::IComponent &component,
			 size_t pin_num_target) = 0;
    
    ///// Print on terminal the name of the component and
    //// the state of every pin of the current component
    /// The output won’t be tested, but it may help you
    // as a trace.
    virtual void Dump(void) const = 0;
    
    virtual ~IComponent(void) { }
  };
}

# endif // __ICOMPONENT_HPP__
